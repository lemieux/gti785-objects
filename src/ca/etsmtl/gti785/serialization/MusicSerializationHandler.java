package ca.etsmtl.gti785.serialization;

import java.util.ArrayList;
import java.util.Collection;

import com.thoughtworks.xstream.XStream;

import ca.etsmtl.gti785.client.responses.MusicListResponse;
import ca.etsmtl.gti785.client.responses.MusicResponse;
import ca.etsmtl.gti785.objects.Music;

public class MusicSerializationHandler {

	private XStream xstream = new XStream();

	/**
	 * Transform an xml string into a instance of Music
	 * @param xml
	 * @return
	 */
	public Music deserializeObject(String xml) {
		xml = xml.replace("<response>", "").replace("</response>", "");
		xstream.alias("data", MusicResponse.class);
		xstream.alias("song", Music.class);
		MusicResponse response = (MusicResponse) xstream.fromXML(xml);
		return response.getSong();
	}

	/**
	 * Transforms a Music instance into a xml string
	 * @param object
	 * @return
	 */
	public String serializeObject(Music object) {
		xstream.alias("song", Music.class);
		return xstream.toXML(object);
	}

	/**
	 * Transforms a list of Music instances into a xml string
	 * @param list
	 * @return
	 */
	public String serializeList(Collection<Music> list) {
		String output = "<songs>";

		for (Music item : list) {
			output += serializeObject(item);
		}

		output += "</songs>";

		return output;
	}

	/**
	 * Transforms an xml string into a list of Music instances
	 * @param xml
	 * @return
	 */
	public Collection<Music> deserializeList(String xml) {
		xml = xml.replace("<response>", "").replace("</response>", "");
		xstream.alias("data", MusicListResponse.class);
		xstream.alias("songs", ArrayList.class);
		xstream.alias("song", Music.class);
		MusicListResponse response = (MusicListResponse) xstream.fromXML(xml);
		return response.getSongs();
	}

}
