package ca.etsmtl.gti785.serialization;

import java.util.ArrayList;
import java.util.Collection;

import ca.etsmtl.gti785.client.responses.PlaylistResponse;
import ca.etsmtl.gti785.objects.Music;

import com.thoughtworks.xstream.XStream;

public class PlaylistSerializationHandler {

	private XStream xstream = new XStream();
	
	/**
	 * Get a playlist from a playlist response
	 * @param xml
	 * @return
	 */
	public Collection<Music> deserializeList(String xml){
		xml = xml.replace("<response>", "").replace("</response>", "");
		xstream.alias("data", PlaylistResponse.class);
		xstream.alias("songs", ArrayList.class);
		xstream.alias("song", Music.class);
		PlaylistResponse response = (PlaylistResponse)xstream.fromXML(xml);
		return response.getSongs();
	}
}
