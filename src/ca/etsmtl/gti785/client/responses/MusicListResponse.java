package ca.etsmtl.gti785.client.responses;

import java.util.ArrayList;

import ca.etsmtl.gti785.objects.Music;

public class MusicListResponse {
	private ArrayList<Music> songs;

	/**
	 * Return the songs contained in the response
	 * @return
	 */
	public ArrayList<Music> getSongs() {
		return songs;
	}

	/**
	 * Set the songs for the response
	 * @param songs
	 */
	public void setSongs(ArrayList<Music> songs) {
		this.songs = songs;
	}
}
