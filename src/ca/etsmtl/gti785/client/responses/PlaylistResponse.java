package ca.etsmtl.gti785.client.responses;

import java.util.Collection;

import ca.etsmtl.gti785.objects.Music;

public class PlaylistResponse {

	private Collection<Music> songs;

	/**
	 * Return the songs contained in the response
	 * 
	 * @return
	 */
	public Collection<Music> getSongs() {
		return songs;
	}

	/**
	 * Set the songs for the response
	 * 
	 * @param songs
	 */
	public void setSongs(Collection<Music> songs) {
		this.songs = songs;
	}

}
