package ca.etsmtl.gti785.client.responses;

import ca.etsmtl.gti785.objects.Music;

public class MusicResponse {
	private Music song;

	/**
	 * Return the song in the response
	 * @return
	 */
	public Music getSong() {
		return song;
	}

	/**
	 * Set the song for the response
	 * 
	 * @param song
	 */
	public void setSong(Music song) {
		this.song = song;
	}
}
