package ca.etsmtl.gti785.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;

import ca.etsmtl.gti785.client.interfaces.MediaPlayer;
import ca.etsmtl.gti785.objects.Music;
import ca.etsmtl.gti785.serialization.MusicSerializationHandler;

public class MediaPlayerClient extends HttpClient implements MediaPlayer {

	private String serverBaseURL;
	private String musicURL;
	private String playlistURL;
	private String playerURL;

	public MediaPlayerClient(String serverURL) {
		serverBaseURL = "http://" + serverURL + "/ManETS_Server";
		musicURL = serverBaseURL + "/music";
		playlistURL = serverBaseURL + "/playlist";
		playerURL = serverBaseURL + "/player";
	}

	@Override
	public Collection<Music> getSongs() throws Exception {
		String response = doGet(musicURL);

		MusicSerializationHandler handler = new MusicSerializationHandler();
		return handler.deserializeList(response);

	}

	@Override
	public Music getSong(String id) throws Exception {
		String url = musicURL + "/" + id;
		String response = doGet(url);

		MusicSerializationHandler handler = new MusicSerializationHandler();
		return handler.deserializeObject(response);
	}

	@Override
	public Boolean addToPlaylist(Music song) throws ClientProtocolException,
			IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("id", song.getId()));
		doPost(playlistURL, values);
		return true;
	}

	@Override
	public Boolean removeFromPlaylist(Music song)
			throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("id", song.getId()));
		doDelete(playlistURL, values);
		return true;
	}

	@Override
	public Boolean clearPlaylist() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("id", "*"));
		doDelete(playlistURL, values);
		return true;
	}

	@Override
	public Boolean stop() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "stop"));
		doPost(playerURL, values);
		return true;
	}

	@Override
	public Boolean play(Music song) throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "play"));
		values.add(new BasicNameValuePair("id", song.getId()));
		doPost(playerURL, values);
		return true;
	}

	@Override
	public Music previous() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "previous"));
		String response = doPost(playerURL, values);
		MusicSerializationHandler handler = new MusicSerializationHandler();
		return handler.deserializeObject(response);
	}

	@Override
	public Music next() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "next"));
		values.add(new BasicNameValuePair("action", "previous"));
		String response = doPost(playerURL, values);
		MusicSerializationHandler handler = new MusicSerializationHandler();
		return handler.deserializeObject(response);
	}

	@Override
	public Boolean play() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "play"));
		doPost(playerURL, values);

		return true;
	}

	@Override
	public void stream() throws ClientProtocolException, IOException {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("action", "stream"));
		doPost(playerURL, values);

	}

}
