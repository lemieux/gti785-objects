package ca.etsmtl.gti785.client.interfaces;

import java.io.IOException;
import java.util.Collection;

import org.apache.http.client.ClientProtocolException;

import ca.etsmtl.gti785.objects.Music;

public interface MediaPlayer {
	/**
	 * Get all songs
	 * 
	 * @return
	 * @throws Exception
	 */
	public Collection<Music> getSongs() throws Exception;

	/**
	 * Get a song by its ID
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Music getSong(String id) throws Exception;

	/**
	 * Add a song to the server playlist
	 * 
	 * @param song
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean addToPlaylist(Music song) throws ClientProtocolException,
			IOException;

	/**
	 * Remove a song from the playlist
	 * 
	 * @param song
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean removeFromPlaylist(Music song)
			throws ClientProtocolException, IOException;

	/**
	 * Clears the playlist
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean clearPlaylist() throws ClientProtocolException, IOException;

	/**
	 * Stop the current playback
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean stop() throws ClientProtocolException, IOException;

	/**
	 * Play the previous song in the playlist
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Music previous() throws ClientProtocolException, IOException;

	/**
	 * Play the next song in the playlist
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Music next() throws ClientProtocolException, IOException;

	/**
	 * Play a specific song
	 * 
	 * @param song
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean play(Music song) throws ClientProtocolException, IOException;

	/**
	 * Play the current song in the playlist
	 * 
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public Boolean play() throws ClientProtocolException, IOException;

	/**
	 * Tell the server to start the stream
	 * 
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public void stream() throws ClientProtocolException, IOException;
}
