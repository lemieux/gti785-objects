package ca.etsmtl.gti785.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

@SuppressWarnings("deprecation")
public class HttpClient {

	private org.apache.http.client.HttpClient client = new DefaultHttpClient();;

	/**
	 * Executes a HTTP GET call
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public String doGet(String url) throws Exception {
		HttpGet get = new HttpGet(url);
		HttpResponse response = client.execute(get);
		String output = "";
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				output += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;

	}

	/**
	 * Execute a HTTP POST call without parameters
	 * @param url
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String doPost(String url)
			throws ClientProtocolException, IOException {
	
		return doPost(url, new ArrayList<NameValuePair>());
	}
	
	/**
	 * Execute a HTTP POST call with parameters
	 * @param url
	 * @param values
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String doPost(String url, List<NameValuePair> values)
			throws ClientProtocolException, IOException {
		HttpPost post = new HttpPost(url);

		UrlEncodedFormEntity params = new UrlEncodedFormEntity(values);
		post.setEntity(params);
		HttpResponse response = client.execute(post);
		String output = "";
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				output += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;
	
	}

	/**
	 * Execute a HTTP DELETE call without parameters
	 * @param url
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String doDelete(String url) throws ClientProtocolException, IOException{
		return doDelete(url, null);
	}
	
	/**
	 * Execute a HTTP DELETE call with parameters
	 * @param url
	 * @param values
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public String doDelete(String url, List<NameValuePair> values) throws ClientProtocolException,
			IOException {
		HttpDelete delete = new HttpDelete(url);
		HttpResponse response = client.execute(delete);
		
		String output = "";
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				output += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return output;

	}

}
