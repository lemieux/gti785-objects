package ca.etsmtl.gti785.objects;

public class Music {
	private String id;
	private String artist;
	private String title;
	private transient String path;

	/**
	 * Returns the song id
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the song id
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the title
	 * 
	 * @return
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Return the artist
	 * 
	 * @return
	 */
	public String getArtist() {
		return artist;
	}

	/**
	 * Set the artist
	 * 
	 * @param artist
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}

	/**
	 * Return the path
	 * 
	 * @return
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Set the path
	 * 
	 * @param path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Return the filename from the path
	 * 
	 * @return
	 */
	public String getFilename() {
		int idx = path.lastIndexOf("/");
		return idx >= 0 ? path.substring(idx + 1) : path;
	}

}
